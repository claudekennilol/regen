# Change Log

## 0.3.1

- Fixed: Fast healing applied to both nonlethal damage and lost health when it should only apply to one.
- Internal restructuring.
- Foundry 0.8 support removed.

## 0.3.0.4

- Fixed: Negative regeneration/fasthealing was treated as positive.

## 0.3.0.3

- Fixed: Bad permission checking.

## 0.3.0.2

- Fixed: Weird behaviour caused by players sometimes handling actors they have no permissions to.

## 0.3.0.1

- Fixed: Regeneration did not work for anything but nonlethal damage.

## 0.3.0

- Changed: Bleeding condition is no longer treated as dying condition.
- Changed: Dying is handled via buff with `dying` boolean flag.
- Changed: Bleeding buff no longer requires the bleeding condition to be enabled on top.
- New: Health Conditions compendium pack with Dying & Bleeding example buffs.

## 0.2.0.1

- Fixed: Defeated combatants were not ignored correctly.
- Fixed: Disabled bleeding buffs were still taken into account.

## 0.2.0

- Fix: Regeneration no longer incorrectly disables bleed.
- New: Bleeding damage via bleed buff (requires `bleeding` boolean flag set, amount configured via buff level).
- New: Stabilization check option.
- Removed: Foundry 0.7.x support in anticipation of 0.9
- Removed: Various settings.

## 0.1.7.1

- Fixed: User retrieval for whisper targets was attempting to get actors instead.

## 0.1.7

- Foundry 0.7.x cross-compatibility
- Changed: Chat card generation and compacting.

## 0.1.6

- Regen state update merged into actor health update, may have significant performance benefit.
- Defeated combatants bail out of the logic earlier to avoid unnecessary processing. Insignificant performance boost.
- Foundry 0.8 compatibility.

## 0.1.5

- Fixed: Healing triggering multiple times on same round (changing initiative, combatants being added or removed, or cycling turn back and forth).
- Fixed: Healing triggering before combat has started.

## 0.1.4

- New: Configuration dialog.
- New: Translation files.

## 0.1.3

- Stopgap fix for bad behaviour. Actual cause unknown.

## 0.1.2

- Hotfix. You saw nothing.

## 0.1.1

### New

- Bleeding out now marks defeated.
- Full transparency option to not hide chat cards.
- Messages are now compacted.

### Fixed

- Handling is now done on GM side solely, fixing variety of horrifying issues.
- Dead characters don't continue to bleed. There's only so much blood.
