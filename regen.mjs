import { CFG, getActiveOwners, getOneOwner, setDefeated } from './module/common.mjs';
import { RegenConfig } from './module/configData.mjs';
import { RegenConfigUI } from './module/config.mjs';
import { getSettings } from './module/utility.mjs';
import { generateStabilizationCard, generateChatCard, generateBleedoutCard, compactChatMessage } from './module/chat.mjs';
import { RegenValue, Bleeding, HealthState, ConditionState } from './module/data.mjs';

const mt = 'REGENERATION';

// TODO: Staggered condition at effective 0 HP.
// TODO: Unstagger at effective >0 HP.

CFG.cardTemplate = `modules/${CFG.module}/template/chatcard.hbs`;
CFG.stabilizationTemplate = `modules/${CFG.module}/template/stabilization.hbs`;
Hooks.once('ready', async () => loadTemplates([CFG.cardTemplate, CFG.stabilizationTemplate]));

const signNum = (num) => `${(num >= 0 ? '+' : '') + num}`; // add + to positive numbers

/**
 * Make sure number is actually a number.
 */
const sanitizeNum = (num) => Number.isFinite(num) ? num : 0;

function sanityCheck(updateData) {
	const numbers = ['data.attributes.hp.value', 'data.attributes.hp.nonlethal', 'data.attributes.hp.temp'];
	const booleans = ['data.attributes.conditions.bleed'];

	const foundNonNumber = numbers.find(k => k in updateData && typeof updateData[k] !== 'number');
	const nB = booleans.find(k => k in updateData && typeof updateData[k] !== 'boolean');
	if (nB || foundNonNumber) {
		console.error('ERROR', nB, updateData[nB], foundNonNumber, updateData[foundNonNumber]);
		throw 'REGENERATION: Internal Error';
	}
}

/**
 * @param {Actor} actor
 * @param {Number} time
 * @param {Object} d
 */
function applyRegen(actor, time, d = {}) {

}

/**
 * @param {Combat} combat
 * @param {Object} _round
 * @param {Object} _options
 * @param {String} _userId
 */
async function regenOnCombatUpdate(combat, _round, _options, userId) {
	if (!combat.started) return;
	const combatant = combat.combatant,
		actor = combatant.actor;
	if (!actor?.isOwner) return; // Combatant has no actor

	if (actor.hasPlayerOwner) {
		const owner = getOneOwner(actor);
		if (owner && owner.id !== game.user.id) return;
	}

	const defeated = combat.combatant?.isDefeated ?? true;
	if (defeated) return; // Don't process tokens that are defeated.

	// TODO: Make this happen on the side of the token's owner where possible instead of doing it only on GM side
	// ^ Debatable usefulness. GM is already present since combat was updated.

	const actorData = combatant.actor.data.data;

	const d = {
		combat,
		get combatant() { return this.combat?.combatant; },
		get actor() { return this.combatant?.actor; },
		get token() { return this.combatant?.token?.object; }, // TokenPF
		fastheal: new RegenValue,
		regen: new RegenValue,
		bleeding: false,
		bleed: new Bleeding,
		dying: false,
		past: new HealthState(actorData.attributes.hp),
		post: new HealthState,
		delta: new HealthState,
		conditions: new ConditionState,
		data: { round: combat.current.round },
		stabilization: { roll: null, dc: null, stabilized: true },
	};

	d.bleeding = actor.hasItemBooleanFlag(CFG.FLAGS.bleeding);
	d.dying = actor.hasItemBooleanFlag(CFG.FLAGS.dying);

	const lastState = actor.getFlag(CFG.module, CFG.FLAGS.combatState);
	if (lastState?.id === combat.id && lastState?.round === combat.round)
		return console.warn(`${mt} | Already applied to ${actor.name} this round.`);

	// Sanitize PF1 data
	d.past.value = sanitizeNum(d.past.value);
	d.past.temp = sanitizeNum(d.past.temp);
	d.past.nonlethal = sanitizeNum(d.past.nonlethal);

	// --- Collect data ---
	const cfg = getSettings();
	if (cfg.debug) {
		console.group(mt);
		console.log(`${mt} | Settings:`, cfg);
	}

	try {
		if (cfg.debug) console.log('Actor:', actor.name, actor.id, actor);
		d.past.bleeding = d.conditions.bleeding = actorData.attributes.conditions.bleed;
		d.past.staggered = d.conditions.staggered = actorData.attributes.conditions.staggered;

		if (cfg.debug) console.group('Regeneration');
		d.regen.parse(actorData.traits.regen, cfg);
		if (cfg.debug) console.groupEnd();

		if (cfg.debug) console.group('Fast healing');
		d.fastheal.parse(actorData.traits.fastHealing, cfg);

		if (cfg.debug) {
			console.groupEnd();
			console.log('Initial state:', d);
		}
	}
	finally {
		if (cfg.debug) console.groupEnd();
	}

	// if (!token.actorLink) return; // don't handle unlinked tokens
	// TODO: Hook into options.advanceTime if it's ever used?

	const buffs = actor.items.filter(b => b.type === 'buff' && b.isActive);

	// --- Process everything ---
	let newHP = d.past.value, newNL = d.past.nonlethal, newT = d.past.temp;

	// Automatic bleed and stabilization
	d.stabilization.dc = 10 - newHP;
	if (cfg.autoBleed) {
		// Dying
		if (d.dying) {
			d.stabilization.stabilized = false;

			// Stabilize
			if (cfg.autoStabilize) {
				d.stabilization.roll = await actor.rollAbilityTest('con', { chatMessage: false });
				d.stabilization.success = d.stabilization.roll.total >= d.stabilization.dc;
				if (d.stabilization.success) {
					if (cfg.debug) console.log('Stabilization success:', d.stabilization.roll.total, 'vs', d.stabilization.dc);
					// TODO: Disable dying buff
					// d.conditions.bleeding = false;
					d.stabilization.stabilized = true;
				}
				if (cfg.debug) console.log('Stabilization:', { total: d.stabilization.roll.total, dc: d.stabilization.dc }, { roll: d.stabilization.roll });

				if (cfg.stabilizeCard)
					await generateStabilizationCard(d, cfg)
						.then(cm => ChatMessage.create(cm));
			}

			// Apply bleeding damage
			if (!d.stabilization.stabilized)
				d.bleed.adjust(-1);
		}

		// Process bleeding buffs
		if (d.bleeding) {
			if (cfg.debug) console.group('Bleeding');
			try {
				const worstBleed = buffs
					.filter(b => b.hasItemBooleanFlag(CFG.FLAGS.bleeding))
					.reduce((l, c) => {
						const level = parseInt(c.data.data.level, 10);
						if (cfg.debug) console.log('Instance:', -level, c.name);
						return Math.min(l, Number.isFinite(level) ? -level : 0);
					}, 0);
				d.bleed.volume = worstBleed;
				if (cfg.debug) console.log('Final:', worstBleed);
			}
			finally {
				if (cfg.debug) console.groupEnd();
			}
		}
	}

	// Apply bleeding
	if (d.bleed.volume < 0) {
		let bleedRemaining = -d.bleed.volume;
		// Bleed goes from temp HP first if available
		let bleedFromTHP = Math.min(bleedRemaining, newT);
		if (newT > 0) {
			newT -= bleedRemaining;
			if (newT < 0) {
				bleedRemaining = -newT;
				bleedFromTHP -= newT;
				newT = 0;
			}
			else bleedRemaining = 0;
		}
		const bleedFromHP = bleedRemaining;
		if (cfg.debug) console.log('>>> Actual Bleeding:', bleedFromHP, 'HP;', bleedFromTHP, 'THP');
		newHP -= bleedRemaining;
	}

	let leftoverHealing = d.regen.value;

	let dead = false, diedNow = false;
	if (cfg.autoDefeat) {
		dead = newHP + newT + leftoverHealing <= -actorData.abilities.con.total;
		diedNow = newHP + newT + leftoverHealing === -actorData.abilities.con.total;
	}

	if (dead && !diedNow && d.regen.value <= 0) return; // They're kinda dead

	// Fast healing does not work when dead
	if (!dead) leftoverHealing += d.fastheal.value;

	if (leftoverHealing === 0 && !d.dying && !d.bleeding && !d.past.bleeding && !d.stabilization.roll) {
		if (cfg.debug) console.log(`+++ ${mt} | ${actor.name} [${actor.id}] is a sad puppy and gets no cookie.`);
		return;
	}

	const potentialHealing = leftoverHealing, updateData = { };

	if (d.dying && d.stabilization.stabilized) {
		// Disable dying buff
		const dying = buffs.find(b => b.hasItemBooleanFlag(CFG.FLAGS.dying));
		await dying.update({ 'data.active': false });
	}

	let actualHPChange = 0, actualNLChange = 0;

	// Nonlethal healing
	let leftoverNLHealing = potentialHealing;
	if (d.past.nonlethal > 0) {
		newNL -= potentialHealing;
		if (newNL < 0) {
			leftoverNLHealing = -newNL;
			newNL = 0;
		}
		else leftoverNLHealing = 0;
		actualNLChange = potentialHealing - leftoverNLHealing;
		leftoverHealing -= actualNLChange;
		// leftoverHealing -= potentialHealing - leftoverNLHealing;
		d.post.nonlethal = newNL;
	}

	// Normal health healing
	if (newHP < d.past.max) {
		newHP += leftoverHealing;
		actualHPChange = Math.min(newHP, d.past.max) - d.past.value;
		if (newHP > d.past.max) {
			leftoverHealing = newHP - d.past.max;
			newHP = d.past.max;
		}
		else leftoverHealing = 0;
		d.post.value = newHP;
	}

	// Update HP
	let hpChange = '', nlChange = '', tChange = '';
	if (newNL !== d.past.nonlethal) {
		nlChange = game.i18n.localize('MKAh.Regeneration.HPChange').format(d.past.nonlethal, newNL);
		updateData['data.attributes.hp.nonlethal'] = newNL;
	}
	if (newHP !== d.past.value) {
		hpChange = game.i18n.localize('MKAh.Regeneration.HPChange').format(d.past.value, newHP);
		updateData['data.attributes.hp.value'] = newHP;
	}
	if (newT !== d.past.temp) {
		tChange = game.i18n.localize('MKAh.Regeneration.HPChange').format(d.past.temp, newT);
		updateData['data.attributes.hp.temp'] = newT;
	}

	// --- Update Actor ---
	if (Number.isNaN(newHP) || Number.isNaN(newT) || Number.isNaN(newNL)) {
		console.error(`!!! ${mt} | SOMETHING WENT WRONG:`, actor.name, d, newHP, newT, newNL);
		return;
	}

	d.post.value = newHP;
	d.post.nonlethal = newNL;
	d.post.temp = newT;
	mergeObject(d.post, { value: newHP, nonlethal: newNL, temp: newT });

	sanityCheck(updateData);

	// setFlag replacement to avoid effective double update
	updateData[`flags.${CFG.module}.${CFG.FLAGS.combatState}`] = { id: combat.id, round: combat.round };
	await actor.update(updateData);

	// --- Output ---

	d.delta = {
		value: d.post.value - d.past.value,
		nonlethal: -(d.past.nonlethal - d.post.nonlethal),
		temp: d.post.temp - d.past.temp,
	}
	if (cfg.debug) console.log(`${mt} | DELTA:`, d.delta);

	const actualTHPChange = newT - d.past.temp;

	const output = [
		`+++ ${mt} | ${actor.name} [${actor.id}] regenerates`,
		`${signNum(actualHPChange)} HP`,
		`${hpChange}`, // HP
		`${signNum(-actualNLChange)} NL`,
		`${nlChange}`, // NL
		`${signNum(-actualTHPChange)} THP`,
		`${tChange}`, // ThP
	];

	if (cfg.autoBleed) {
		const bled = d.past.bleeding !== d.conditions.bleeding;
		const bChange = `Bled ${d.bleed.volume}`;
		if (d.bleed.volume < 0) output.push(bChange);
		if (bled) output.push(game.i18n.localize('MKAh.Bleed.Stopped'));
	}
	if (d.conditions.bleeding) output.push(game.i18n.localize('MKAh.Bleed.Ongoing'));
	if (dead) output.push(game.i18n.localize('MKAh.State.Defeated'));

	// Combine output and print to console
	console.log(output.filter(i => i.length).join('; '), '\n', { data: d });

	if (d.delta.value !== 0 || d.delta.temp !== 0 || d.delta.nonlethal !== 0) {
		if (cfg.debug) {
			console.group(mt);
			console.log('Awaken:', d.past.effective, '<', d.past.nonlethal, '=', d.past.effective < d.past.nonlethal,
				'&', d.post.effective, '>=', newNL, '=', d.post.effective >= newNL);
			console.log('LEFTOVER', leftoverHealing, 'POTENTIAL', potentialHealing, 'ACTUAL', potentialHealing - leftoverHealing);
			console.log('Full Data:', d)
			console.groupEnd();
		}

		// Print a chatcard about the changes
		if (cfg.chatcard) {
			const cm = await generateChatCard(d, cfg);
			await ChatMessage.create(cm);
		}
	}

	if (diedNow) {
		// Bled out chat card
		if (cfg.bleedoutCard) {
			const cm = await generateBleedoutCard(d, cfg);
			await ChatMessage.create(cm);
		}

		// Automatic defeat
		if (cfg.autoDefeat && !d.combatant.data.defeated)
			await setDefeated(d, true);
	}
}

/**
 * TODO: Regen with time passing.
 *
 * @param {Number} worldTime
 * @param {Number} delta
 */
function regenOnTimePassing(worldTime, delta) {
	// console.log('World Time Changed:', delta < 0 ? delta : `+${delta}`);
}

Hooks.on('renderChatMessage', compactChatMessage);

Hooks.once('init', () => {
	game.settings.register(CFG.module, 'config', {
		scope: 'world',
		type: Object,
		config: false,
		default: RegenConfig.default(),
	});

	game.settings.registerMenu(CFG.module, 'config', {
		id: 'mkah-pf1-regen-config',
		label: 'MKAh.Regeneration.Config.Label',
		name: 'MKAh.Regeneration.Config.Title',
		hint: 'MKAh.Regeneration.Config.Hint',
		scope: 'world',
		icon: 'fas fa-tint',
		type: RegenConfigUI,
		config: true,
		restricted: true,
	});

	Hooks.on('updateCombat', regenOnCombatUpdate);
	Hooks.on('updateWorldTime', regenOnTimePassing);
});
