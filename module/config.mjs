import { CFG } from './common.mjs';
import { getSettings } from './utility.mjs';
import { RegenConfig } from './configData.mjs';

class RegenConfigUI extends FormApplication {
	regen = undefined;

	constructor(object, options) {
		super(object, options);

		this.regen = getSettings();
	}

	get template() {
		return `modules/${CFG.module}/template/config.hbs`;
	}

	static get defaultOptions() {
		return mergeObject(super.defaultOptions, {
			id: 'pf1-regen-config',
			title: game.i18n.localize('MKAh.Regeneration.Config.Title'),
			width: 480,
			// height: "auto",
			closeOnSubmit: false
		});
	}

	getData(options) { // return can be an object or Promise
		const data = super.getData(options);
		data.regen = this.regen;
		return data;
	}

	_onResetDefaults(event) {
		event.preventDefault();

		this.regen = RegenConfig.default();
		ui.notifications.warn('Save changes to finalize reset.');

		this.render(false);
	}

	_updateObject(ev, data) {
		if (this.regen.debug || data.debug) console.log('REGENERATION | _updateObject:\n', arguments);

		let errors = 0;
		data.ignore = data.ignore.split('\n')
			.map(f => f.trim()) // trim
			.filter(f => f.length) // remove empty
			.filter(il => {
				// remove bad regexp
				try {
					'test'.match(il);
					return true; // good enough
				}
				catch (err) {
					console.log('REGENERATION | Bad ignore pattern:', il, err);
					ui.notifications.error('Bad ignore pattern: ' + il, { permanent: true });
					errors++;
					return false;
				}
			});

		if (errors <= 0) {
			const saveData = Object.assign(RegenConfig.default(), data);

			game.settings.set(CFG.module, 'config', saveData);
			console.group('SAVING');
			console.log('Form Data:', data);
			console.log('Saved Data:', saveData);
			console.groupEnd();
			this.close();
		}
	}

	activateListeners(html) {
		super.activateListeners(html);

		html.find('button[name="reset"]').click(this._onResetDefaults.bind(this));
	}
}

export { RegenConfigUI };
