import { CFG, getUsers } from './common.mjs';

/**
 * Combat and prettify chat messages.
 *
 * @param {ChatMessage} cm
 * @param {JQuery} jq
 */
export function compactChatMessage(cm, jq) {
	// const settings = getSettings();
	const flags = cm.data.flags?.[CFG.module];
	if (!flags) return;
	const html = jq[0];

	const wt = html.querySelector('.whisper-to');
	if (wt) wt.style.display = 'none'; // Remove whisper target

	const sc = html.closest('.chat-message');
	if (!sc) return;
	sc.classList.add('koboldworks');
	if (flags.regeneration) sc.classList.add('regeneration');
	if (flags.stabilization) sc.classList.add('stabilization');
	if (flags.bleedout) sc.classList.add('bleedout');

	// Metadata
	const mh = sc.querySelector('.message-header');
	if (mh) {
		const ts = mh.querySelector('.message-timestamp');
		mh.classList.remove('message-header');
		if (ts) ts.style.display = 'none'; // Hide
	}

	// Move contents
	const ms = html.querySelector(`.${CFG.module}`);
	const sender = mh.querySelector('.message-sender');
	if (sender) {
		sender.classList.remove('message-sender');
		sender.classList.add('regenerator');
	}

	// Shuffle
	const body = document.createElement('div');
	body.classList.add('prime-message');
	mh?.prepend(body);
	body.append(sender, ...ms.childNodes);

	html.style.borderColor = '';
}

export function generateBaseChatCard(d, cfg) {
	return {
		type: CONST.CHAT_MESSAGE_TYPES.OOC,
		rollMode: cfg.transparency ? 'roll' : 'gmroll',
		speaker: ChatMessage.getSpeaker({ token: d.token.document }),
		whisper: cfg.transparency ? [] : getUsers(d.actor, CONST.DOCUMENT_PERMISSION_LEVELS.OWNER),
		roll: null,
		flags: { [CFG.module]: {} }
	};
}

// TODO: Allow overriding this somehow?
export async function generateChatCard(d, cfg) {
	d.data.class = CFG.module;
	d.data.displayName = d.token.name;
	d.data.roundLabel = game.i18n.localize('PF1.CombatInfo_Round').format(d.data.round);
	const data = generateBaseChatCard(d, cfg);
	data.flags[CFG.module].regeneration = true;
	data.content = await renderTemplate(CFG.cardTemplate, d);
	return data;
}

export async function generateStabilizationCard(d, cfg) {
	d.data.class = CFG.module;
	d.data.json = escape(JSON.stringify(d.stabilization.roll));
	d.data.dcroll = RollPF.safeRoll(`10[Base] - ${-(d.stabilization.dc - 10)}[Health]`);
	d.data.dcjson = escape(JSON.stringify(d.data.dcroll));
	const data = generateBaseChatCard(d, cfg);
	data.flags[CFG.module].stabilization = true;
	data.content = await renderTemplate(CFG.stabilizationTemplate, d);
	return data;
}

export async function generateBleedoutCard(d, cfg) {
	const message = game.i18n.localize('MKAh.Regeneration.BledOut');
	const data = generateBaseChatCard(d, cfg);
	data.flags[CFG.module].bleedout = true;
	data.content = '<div>' + message + '</div>';
}
