import './utility.mjs';

export class RegenConfig {
	ignore = [
		// /\(.*\)/, // ignore any with parenthesis, hopefully catching most instances with special cases.
		'per (hour|min|day)',
	];

	chatcard = true;
	transparency = false;
	autoDefeat = false;
	autoStabilize = false;
	stabilizeCard = true;
	autoBleed = false;
	debug = false;

	static default() {
		return new this();
	}
}
