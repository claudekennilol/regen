export const CFG = {
	module: 'mkah-pf1-regen',
	FLAGS: {
		combatState: 'combatState',
		bleeding: 'bleeding',
		dying: 'dying',
	},
	cardTemplate: undefined,
	stabilizationTemplate: undefined,
};

/**
 * @param {Actor} actor
 */
export function getActiveOwners(actor) {
	const perm = actor.data.permission;
	return Object.keys(perm).reduce((users, uid) => {
		const user = game.users.get(uid);
		if (user?.active && !user.isGM && actor.testUserPermission(user, 'OWNER'))
			users.push(user);
		return users;
	}, []);
}

export function getOneOwner(actor) {
	const owners = getActiveOwners(actor)
		.sort((a, b) => a.role > b.role ? -1 : 1);
	if (owners.length == 0) return null;
	const highest = owners[0].role;
	const remaining = owners.filter(u => u.role == highest)
		.sort((a, b) => a.id.localeCompare(b.id, null));
	const first = remaining[0];
	return first;
}

export async function setDefeated(d, defeated) {
	try {
		await d.combat.updateEmbeddedDocuments('Combatant', [{ _id: d.combatant.id, defeated }]);
		const status = CONFIG.statusEffects.find(ae => ae.id === CONFIG.Combat.defeatedStatusId);
		const effect = d.token.actor && status ? status : CONFIG.controlIcons.defeated;
		await d.token.toggleEffect(effect, { active: true, overlay: true });
	}
	catch (err) {
		console.error('Error marking combatant defeated:', err);
	}
}

export const getUsers = (thing, permission) => Object
	.entries(thing.data.permission)
	.filter(u => u[1] >= permission)
	.map(u => game.users.get(u[0]))
	.filter(u => u != undefined);
