import { getRegen } from './utility.mjs';

export class RegenValue {
	value = null;
	source = null;

	parse(source, cfg) {
		this.source = source;
		this.value = getRegen(source, cfg);
	}
}

export class HealthState {
	value = null;
	nonlethal = null;
	temp = null;
	bleeding = false;
	max = null;

	constructor({ value = null, nonlethal = null, temp = null, max = null } = {}) {
		this.value = value;
		this.nonlethal = nonlethal;
		this.temp = temp;
		this.max = max;
	}

	get effective() {
		return this.value + this.temp;
	}
}

export class ConditionState {
	bleeding;
	staggered;
}

export class Bleeding {
	volume = 0;

	adjust(value) {
		this.volume += value;
	}
}
