# Regeneration for Pathfinder 1e

Adds automated regeneration, fast healing, and bleeding.

Regeneration automatically stops bleeding condition.

Chat example:  
![PF1 Regen In Chat](./img/screencaps/chat.png)

Console log example:  
![PF1 Regen In Logs](./img/screencaps/log.png)

Optionally automatically stabilizes as well.

## Configuration

1 hit point bleeding from dying condition is processed if bleed condition is enabled.

Greater bleed requires a buff with `bleeding` boolean flag is present and active. The item level corresponds with bleed amount. Only largest bleeding source is applied.

## Limitations

- You may have to undo bleed in some circumstances since the system is not perfect.

## Install

Manifest URL: <https://gitlab.com/mkah-fvtt/pf1/regen/-/raw/latest/module.json>

## Attribution

If you use any of the code in this project, I would appreciate I or the project was credited for inspiration or whatever where appropriate. Or just drop a line about using my code. I do not mind not having this, but it's just nice knowing something has come out of my efforts.

## Donations

[![ko-fi](https://ko-fi.com/img/githubbutton_sm.svg)](https://ko-fi.com/I2I13O9VZ)

## License

This software is distributed under the [MIT License](./LICENSE).
